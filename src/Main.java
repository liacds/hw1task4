import java.util.*;
import java.util.Random;
import java.util.Scanner;
import java.lang.Math;
public class Main {

    public static void main(String[] args) {
        System.out.println("Please enter matrix dimension");
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        int n = scanner.nextInt();
        int[][] matrix = new int[n][n];
        System.out.println("original matrix");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = random.nextInt(100) * (int) Math.pow(-1, random.nextInt(2));
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("Please enter a column number (between 1 and n, n being matrix dimension) to sort the matrix ");
        int k = scanner.nextInt();

        sortByColumn(matrix, k - 1);
        System.out.println("Sorted matrix by column k");
        printMatrix(matrix);

        sumOfElementsBetweenTwoPositiveElements(matrix, n);
        findMaxElement(matrix, n);

    }

    public static void sortByColumn(int[][] matrix, int col) {

        Arrays.sort(matrix, new Comparator<int[]>() {

            public int compare(final int[] entry1,
                               final int[] entry2) {

                if (entry1[col] > entry2[col])
                    return 1;
                else
                    return -1;
            }
        });
    }


    public static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }


public static void sumOfElementsBetweenTwoPositiveElements(int[][] matrix, int n){
    for (int i = 0; i < n; i++) {
        int sum =0;
        int start = 0;
        int end = 0;
        boolean startCounting = false;
        for (int j = 0; j < n; j++) {
            if (matrix[i][j] > 0 && !startCounting) {
                startCounting = true;
                start = matrix[i][j];
            }
            else if (matrix[i][j] > 0 && startCounting) {
                startCounting = false;
                end = matrix[i][j];
                break;
            } else if (startCounting) {
            sum += matrix[i][j];
            }

        }
        System.out.println(i+1 + ".  Elements: "+start+", " +end+ "; Sum:" + sum+";");
    }
}


public static void findMaxElement(int[][] matrix, int n){
        int maxElement = Integer.MIN_VALUE;
        ArrayList<Integer> rows = new ArrayList<Integer>();
    ArrayList<Integer> columns = new ArrayList<Integer>();

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if(matrix[i][j] > maxElement){
                maxElement = matrix[i][j];
            }
        }
    }

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if(matrix[i][j] == maxElement){
                if( !rows.contains(i)){
                    rows.add(i);
                }
                if(!columns.contains(j)){
                    columns.add(j);
                }
            }
        }
    }
    System.out.println("The largest element is: "+ maxElement);
deleteMaxElement(matrix, rows, columns);
}
public static void deleteMaxElement( int[][] matrix, ArrayList<Integer> rows, ArrayList<Integer> columns  ) {
    int[][] matrix2 = new int[matrix.length - rows.size()][matrix.length - columns.size()];

    int p = 0;
    for( int i = 0; i < matrix.length; ++i)
    {
        if ( rows.contains(i))
            continue;


        int q = 0;
        for( int j = 0; j < matrix.length; ++j)
        {
            if ( columns.contains(j))
                continue;

            matrix2[p][q] = matrix[i][j];
            ++q;
        }

        ++p;
    }

    printMatrix(matrix2);
}
}





